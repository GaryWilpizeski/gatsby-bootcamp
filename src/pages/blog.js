import React from "react"
import Layout from "../components/layout"
import { Link, graphql, useStaticQuery } from "gatsby"
import Head from "../components/head"

import blogStyles from "./blog.module.scss"

const BlogPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allContentfulBlogPost(sort: { fields: publishedDate, order: DESC }) {
        edges {
          node {
            title
            slug
            publishedDate(formatString: "MMMM Do, YYYY")
          }
        }
      }
    }
  `)

  return (
    <div>
      <Layout>
        <Head title="Blog" />
        <h1>Blog</h1>
        <p>
          Posts go here. Lorem ipsum dolor sit amet consectetur adipisicing
          elit. Ratione blanditiis odit, illo ab optio vel nostrum dolores
          minima dolore, sapiente laudantium, saepe quas. Earum, ipsum! Debitis
          deserunt beatae omnis quas.
        </p>
        <div className="blogContent">
          <ol className={blogStyles.posts}>
            {data.allContentfulBlogPost.edges.map((edge, i) => {
              return (
                <li key={i} className={blogStyles.post}>
                  <Link to={`/blog/${edge.node.slug}`}>
                    <h2>{edge.node.title}</h2>
                    <p>Date: {edge.node.publishedDate}</p>
                    <p className={blogStyles.excerpt}>
                      Post Excerpt to go here eventually.
                    </p>
                  </Link>
                </li>
              )
            })}
          </ol>
        </div>
      </Layout>
    </div>
  )
}

export default BlogPage
