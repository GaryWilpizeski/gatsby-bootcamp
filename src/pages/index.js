import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Head from "../components/head"

const IndexPage = () => {
  return (
    <Layout>
      <Head title="Home" />
      <h1>Hello.</h1>
      <h2>My name is Gary, a Front End Developer from Philadelphia.</h2>
      <p>
        Need a developer? <Link to="/contact">Contact me</Link>.
      </p>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Saepe
        explicabo tenetur voluptatibus tempore sunt fuga veniam, adipisci
        numquam consectetur velit alias modi iusto ex est quisquam impedit error
        voluptates quia!
      </p>
    </Layout>
  )
}

export default IndexPage
