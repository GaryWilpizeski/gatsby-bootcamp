import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

const AboutPage = () => {
  return (
    <div>
      <Layout>
        <Head title="About" />
        <h1>About Me</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit,
          voluptatem sapiente porro minus possimus necessitatibus quasi sint
          delectus quam totam ipsam incidunt modi, ea assumenda itaque
          doloremque dolor dicta perferendis.
        </p>
        <p>
          <Link to="/contact">Contact me.</Link>
        </p>
      </Layout>
    </div>
  )
}

export default AboutPage
