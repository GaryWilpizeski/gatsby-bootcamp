import React from "react"

import Layout from "../components/layout"
import Head from "../components/head"

const ContactPage = () => {
  return (
    <div>
      <Layout>
        <Head title="Contact" />
        <h1>Contact Me</h1>
        <h3>Gary Wilpizeski</h3>
        <p>
          <b>Address:</b>
          <br />
          Philadelphia, PA 19147
          <br />
          <br />
          <b>Phone:</b>
          <br />
          <a href="tel:123-456-7890" rel="nofollow">
            123-456-7890
          </a>
        </p>
        <p>
          <b>Twitter:</b>{" "}
          <a
            href="https://twitter.com/garr1s0n"
            target="_blank"
            rel="noopener noreferrer"
          >
            @garr1s0n
          </a>
        </p>
      </Layout>
    </div>
  )
}

export default ContactPage
